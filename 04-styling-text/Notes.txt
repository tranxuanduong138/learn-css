color: #DDD
text-align: center|left|right|justify
text-decoration: none|underline|overline|line-through
text-transform: uppercase|lowercase|capitalize
text-indent: 10px
letter-spaceing: 5px
line-height: 1.2 // a number
text-shadow: x y color blur

CSS cheat sheet