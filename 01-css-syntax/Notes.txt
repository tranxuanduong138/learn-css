- A CSS rule-set consists of a selector and a declaration block
- The selector points to the HTML element you want to style
- The declaration block contains one or more declarations 
seperated by semicolons
- Each declaration includes a CSS property name and a value, 
seperated by a colon
- A CSS declaration always ends with a semicolon, and declaration block 
are surrounded by curly braces